package ru.victor.autocomplete;


import net.sourceforge.suggesttree.SuggestTree;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by victor on 03.01.15.
 */
public class Dictionary {
    private SuggestTree suggestTree ;
    private int suggestionSize;
//    public int totalElementCounter;
//    public int totalGroupCounter;

    public Dictionary(int suggestionSize) {
        assert suggestionSize > 0 : "Size can't be less than 1";
        this.suggestionSize = suggestionSize;
        suggestTree = new SuggestTree(suggestionSize+1); //we have to use one extra entry otherwise we can break lexicographically order with the equal weights
    }

    public void addWord(String word, int quantity) {
        assert quantity > 0 : "quantity can't be less than 1";
        suggestTree.put(word, quantity);
    }

    @SuppressWarnings("unused")
    public String getTopMatching(String prefix) {
        StringBuilder sb= new StringBuilder();

        SuggestTree.Node node = suggestTree.getAutocompleteSuggestions(prefix);

        if (node==null)
            return "";
//        System.err.println(node.listLength());
        for (int i = 0; i < node.listLength() && i<suggestionSize; i++) {
            SuggestTree.Entry entry = node.getSuggestion(i);
            sb.append(entry.getTerm())/*.append(" ").append(entry.getWeight())*/.append("\n");
//            totalElementCounter++;
        }
//        totalGroupCounter++;

        return sb.toString();
    }

    @SuppressWarnings("unused")
    public List<String>  getTopMatchingList(String prefix) {
        List<String> result = new LinkedList<>();

        SuggestTree.Node node = suggestTree.getAutocompleteSuggestions(prefix);

        if (node==null)
            return result;
        //        System.err.println(node.listLength());
        for (int i = 0; i < node.listLength() && i<suggestionSize; i++) {
            SuggestTree.Entry entry = node.getSuggestion(i);
            result.add(entry.getTerm());
//            totalElementCounter++;
        }
//        totalGroupCounter++;

        return result;
    }

}
