package ru.victor.autocomplete;

import java.util.Scanner;

public class AutoComplete {
    public static final int RANK_SIZE = 10;

    public static void main(String[] args) {
//        final long startTime = System.currentTimeMillis();

        Scanner scanIn = new Scanner(System.in);

        int dictCount = Integer.parseInt(scanIn.nextLine().trim());
        Dictionary dictionary = new Dictionary(RANK_SIZE);

//        System.err.println(dictCount);

        for (int i = 0; i < dictCount; i++) {
            String in = scanIn.nextLine().trim();
            String str[] = in.split(" ");
            dictionary.addWord(str[0], Integer.parseInt(str[1]));
        }

//        final long warmUpTime = System.currentTimeMillis();

        int keysCount = Integer.parseInt(scanIn.nextLine().trim());
        for (int i = 0; i < keysCount; i++) {
            String prefix = scanIn.nextLine();
            System.out.println(dictionary.getTopMatching(prefix));

//            dictionary.getTopMatchingList(prefix).forEach(System.out::println);
            System.out.println();
        }
        scanIn.close();

//        final long stopTime = System.currentTimeMillis();
//        final long elapsedTime = stopTime - startTime;
//        System.err.println("Warm up time " + (warmUpTime - startTime) + "ms");
//        System.err.println("Took " + elapsedTime + "ms");
//        System.err.println("totalGroupCounter " + dictionary.totalGroupCounter);
//        System.err.println("totalElementCounter " + dictionary.totalElementCounter);

    }
}
