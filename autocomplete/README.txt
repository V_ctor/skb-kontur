You have to install JDK 1.8

to build jar from command line use
build_jar.sh
to run for interactive mode use
run_jar.sh
or
run_jar.sh <"input file"
for batch mode

you can use time command to measure the time it takes
time run_jar.sh <"input file"
